﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTest
{
    class LoginPage
    {
        public LoginPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Name, Using = "UserName")]
        public IWebElement InputUserName { get; set; }

        [FindsBy(How = How.Name, Using = "Password")]
        public IWebElement InputPassword { get; set; }

        [FindsBy(How = How.Name, Using = "Login")]
        public IWebElement LoginButton { get; set; }
    }
}
