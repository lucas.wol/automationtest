﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTest
{
    class UserFormPage
    {
        public UserFormPage()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        [FindsBy(How = How.Id, Using = "TitleID")]
        public IWebElement InputTitle { get; set; }

        [FindsBy(How = How.Name, Using = "Initial")]
        public IWebElement InputInitial { get; set; }

        [FindsBy(How = How.Name, Using = "FirstName")]
        public IWebElement InputFirtsName { get; set; }

        [FindsBy(How = How.Name, Using = "MiddleName")]
        public IWebElement InputMiddleName { get; set; }

        [FindsBy(How = How.Name, Using = "Save")]
        public IWebElement SaveButton { get; set; }
    }
}
