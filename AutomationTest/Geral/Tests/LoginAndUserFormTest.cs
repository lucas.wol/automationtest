﻿using AutomationTest;
using AutomationTest.Geral.Steps;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        [SetUp]
        public void Initialize()
        {
            PropertiesCollection.driver = new ChromeDriver(/*"%automation%"*/"D:\\Testes Automatizados\\AutomationTest\\AutomationTest");
            PropertiesCollection.driver.Navigate().GoToUrl("http://www.executeautomation.com/demosite/Login.html");
            Console.WriteLine("Opnened URL");
        }

        [Test]
        public void ExecuteTest()
        {
            LoginStep login = new LoginStep();
            login.Login("execute", "automation");

            UserFormStep userForm = new UserFormStep();
            userForm.FillUserForm("execute", "Lucas", "Wolfgramm");
        }

        [Test]
        public void NextTest()
        {
            Console.WriteLine("Next Method");
        }

        [TearDown]
        public void CleanUp()
        {
            PropertiesCollection.driver.Close();
            Console.WriteLine("Closed the browser");
        }
    }
}
