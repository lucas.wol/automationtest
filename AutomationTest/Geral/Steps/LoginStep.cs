﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTest.Geral.Steps
{
    class LoginStep
    {
        LoginPage page = new LoginPage();

        public LoginStep Login(string userName, string password)
        {
            page.InputUserName.EnterText(userName);
            page.InputPassword.EnterText(password);
            page.LoginButton.Submit();

            return this;
        }
    }
}
