﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationTest.Geral.Steps
{
    class UserFormStep
    {
        UserFormPage page = new UserFormPage();

        public UserFormStep FillUserForm(string initial, string firstName, string middleName)
        {
            page.InputInitial.EnterText(initial);
            page.InputFirtsName.EnterText(firstName);
            page.InputMiddleName.EnterText(middleName);
            page.SaveButton.Clicks();

            return this;
        }
    }
}
